import React from 'react';

import FaInstagram from 'react-icons/lib/fa/instagram';
import FaFacebookOfficial from 'react-icons/lib/fa/facebook-official';

import { Div } from '../../styles';
import { Body, Title, SubTitle } from './styles';

const App = () => (
  <Body>
    <Div
      display="flex"
      flex-grow="1"
      max-width="760px"
      flex-direction="column"
      justify-content="center"
    >
      <Div display="flex" flex-direction="column" align-items="center">
        <Title>KIAI INDUSTRIES</Title>
        <SubTitle>Play Clean</SubTitle>
      </Div>
    </Div>
    <Div display="flex" flex-direction="row">
      <a href="https://www.instagram.com/kiaiindustries/">
        <FaInstagram size="50" style={{ color: '#000', marginRight: 10 }} />
      </a>
      <a href="https://www.facebook.com/kiaiindustries/">
        <FaFacebookOfficial size="50" style={{ color: '#000', marginLeft: 10 }} />
      </a>
    </Div>
  </Body>
);

export default App;
