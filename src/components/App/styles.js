import styled from 'styled-components';
import flex from '../../styles/flex';
import { media } from '../../styles';

const Body = styled.div`
  ${flex.vertical}
  ${flex.centerHorizontal}
  height: 100%;
`;

const Logo = styled.img`
  width: 150px;
`;

const Title = styled.h1`
  font-size: 72px;
  font-family: 'Alegreya Sans SC', sans-serif;
  ${media.tablet`font-size: 42px;`}
`;

const SubTitle = styled.h2`
  font-size: 42px;
  font-family: 'Pacifico', cursive;
  ${media.tablet`font-size: 32px;`}
`;

const Info = styled.p`
  font-size: 22px;
  font-weight: 400;
`;

export {
  Body,
  Logo,
  Title,
  SubTitle,
  Info,
};
