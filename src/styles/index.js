import styled, { css } from 'styled-components';

const Div = styled.div`
  ${p => Object.keys(p).map(k => `${k}: ${p[k]};`).join('\n')};
`;

const sizes = {
  desktop: 992,
  tablet: 768,
  phone: 450
}

// media query template
const media = Object.keys(sizes).reduce((acc, label) => {
  acc[label] = (...args) => css`
    @media (max-width: ${sizes[label] / 16}em) {
      ${css(...args)}
    }
  `

  return acc
}, {});


export {
  Div,
  media,
};
