## Kiai Industries Construction Page

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

###### TODO

- [x] Structure of styled components
- [ ] Social links
- [x] Add express for simple deployment
- [ ] Combine express and client in same repo
- [ ] Responsive typography
